import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public pokemonData: any;
  constructor(private _ar: ActivatedRoute) {
    _ar.data.subscribe((x) => {
      this.pokemonData = x.data;
      console.log(this.pokemonData);
    });
  }

  ngOnInit(): void {}
}
