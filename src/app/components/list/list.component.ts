import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public pokemonList: any[] = [];
  constructor(private _ar: ActivatedRoute) {
    this._ar.data.subscribe((x) => {
      this.pokemonList = x.data.results;
      console.log(this.pokemonList);
    });
  }

  ngOnInit(): void {}
}
