import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PokeApiService {
  constructor(private _http: HttpClient) {}

  public getPokemonList(
    limit: number = 20,
    offset: number = 0
  ): Observable<any> {
    return this._http.get(
      `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`
    );
  }

  public getSinglePokemonData(id: number): Observable<any> {
    return this._http.get(`https://pokeapi.co/api/v2/pokemon/${id}`);
  }
}
