import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './components/detail/detail.component';
import { ListComponent } from './components/list/list.component';
import { PokemonDetailService } from './resolvers/pokemonDetail/pokemon-detail.service';
import { PokemonListService } from './resolvers/pokemonList/pokemon-list.service';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    resolve: {
      data: PokemonListService,
    },
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    resolve: {
      data: PokemonDetailService,
    },
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
