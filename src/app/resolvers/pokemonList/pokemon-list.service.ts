import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PokeApiService } from 'src/app/services/poke-api/poke-api.service';

@Injectable({
  providedIn: 'root',
})
export class PokemonListService implements Resolve<any> {
  constructor(private _ps: PokeApiService, private _poke: PokeApiService) {}
  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this._poke.getPokemonList();
  }
}
