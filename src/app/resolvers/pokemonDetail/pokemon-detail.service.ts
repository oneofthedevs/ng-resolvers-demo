import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { PokeApiService } from 'src/app/services/poke-api/poke-api.service';

@Injectable({
  providedIn: 'root',
})
export class PokemonDetailService implements Resolve<any> {
  constructor(private _poke: PokeApiService) {}
  resolve(route: ActivatedRouteSnapshot) {
    let id = route.paramMap.get('id') ?? 1;
    id = +id;
    return this._poke.getSinglePokemonData(id);
  }
}
